import express from 'express';
import calculateRouter from './src/calculate/router.js';
import testRouter from './src/test/router.js';
/**
 * we need to create an express app to calculate numbers
 * we need to define an endpoint to operate calculations as following
 * 1. endpoint gonna have POST/calculate path
 * 2. endpoint gonna receive the following body 
 * {
 *      x: number,
 *      y: number,
 *      operation: string
 * }
 * 3. validate x and y as numbers
 * 4. validate operation as one of the following operations
 * [+,-,/,*]
 * 5. return the result as the following object
 * {
 *      result: result 
 * }
 * 
*/

const app = express();
// extract json data
app.use(express.json())

app.use('/calculate', calculateRouter)
app.use('/test', testRouter)

// error handling
app.use((error, req, res, next) => {
  console.log(error.message)
  res.status(error.statusCode).send({
    status: false,
    message: error.message,
    stack: error.stack,
    statusCode: error.statusCode,
    errors: error.errors
  })
})
app.listen(3000, () =>
  console.log(`🚀 Server ready at: http://localhost:3000`)
);
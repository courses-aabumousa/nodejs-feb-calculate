import { calculate } from './service.js'
export const calculateController = (req, res) => {
  // logic middleware
  try {
    const { x, y, operation } = req.body;
    const result = calculate(x, y, operation);
    res.send({
      result,
    });
  } catch (e) {
    // error code 500
    res.status(500);
    res.send({
      success: false,
      message: e.message,
    });
  }
};

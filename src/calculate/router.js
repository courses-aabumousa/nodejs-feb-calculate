import express from "express";
import { body } from "express-validator";
import { calculateController } from "./controller.js";
import { validationResultMiddleware } from "../validators.js";

// create router instance
const router = express.Router();

router.post(
  "/",
  [
    body("operation")
      .exists()
      .withMessage("operation not existed")
      .bail()
      .isIn(["/", "*", "+", "-"])
      .withMessage("invalid operation"),
    body("x").exists().isInt().withMessage("invalid number"),
    body("y").exists().isInt().withMessage("invalid number"),
  ],
  validationResultMiddleware,
  calculateController
);

export default router;

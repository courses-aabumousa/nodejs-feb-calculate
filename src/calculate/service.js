export const calculate = (x, y, operation) => {
    if (x > 100) {
        throw new Error('x exceeding hundred')
    }
    if (y === 0 && operation === '/') {
        throw new Error('un allowed operation')
    }
    switch (operation) {
        case '+': 
            return x + y;
        case '-':
            return x - y;
        case '*':
            return x * y;
        case '/':
            return x / y;
        case '%':
            return x % y;
    }

}
